package in.ijasnahamed.locationupdater.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SharePreferences {
    private SharedPreferences prefObject;

    private final String sharedPrefFile = "LocationUpdatedPrefFile";
    private final String jobIdKey = "jobId";
    private final String scanDetailsKey = "scanDetails";
    private final String locationUpdatesKey = "locationUpdates";

    public SharePreferences(Context context) {

        prefObject = context.getSharedPreferences(
                sharedPrefFile,
                Context.MODE_PRIVATE
        );
    }



    private int getIntValue(String key) {
        return prefObject.getInt(key, -1);
    }

    private void setIntValue(String key, int value) {
        SharedPreferences.Editor editor = prefObject.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private String getStringValue(String key) {
        return prefObject.getString(key, null);
    }

    private void setStringValue(String key, String value) {
        SharedPreferences.Editor editor = prefObject.edit();
        editor.putString(key, value);
        editor.apply();
    }



    public void setJobId(int jobId) {
        setIntValue(jobIdKey, jobId);
    }

    public int getJobId() {
        return getIntValue(jobIdKey);
    }

    public void setLocationUpdates(JSONArray updates) {
        setStringValue(locationUpdatesKey, updates.toString());
    }

    public JSONArray getLocationUpdates() {
        String updates = getStringValue(locationUpdatesKey);

        JSONArray result;
        if(updates != null) {
            try {
                result = new JSONArray(updates);
            } catch (JSONException e) {
                result = new JSONArray();
            }
        } else {
            result = new JSONArray();
        }

        return result;
    }

    public void setScanDetails(JSONObject details) {
        setStringValue(scanDetailsKey, details.toString());
    }

    public JSONObject getScanDetails() {
        String details = getStringValue(scanDetailsKey);

        JSONObject result;
        if(details != null) {
            try {
                result = new JSONObject(details);
            } catch (JSONException e) {
                result = new JSONObject();
            }
        } else {
            result = new JSONObject();
        }
        return result;
    }
}
