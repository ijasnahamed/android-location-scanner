package in.ijasnahamed.locationupdater.utils;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import in.ijasnahamed.locationupdater.background.LocationJobService;

public class JobSchedulerUtils {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static Boolean schedule(Context context, int jobId, int minLatency, int maxLatency) {
        ComponentName jobComponent = new ComponentName(context, LocationJobService.class);

        JobInfo.Builder builder = new JobInfo.Builder(jobId, jobComponent);
        builder.setMinimumLatency(minLatency);
        builder.setOverrideDeadline(maxLatency);

        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if(scheduler == null)
            return false;

        scheduler.cancel(jobId);
        return (scheduler.schedule(builder.build()) == JobScheduler.RESULT_SUCCESS);
    }
}
