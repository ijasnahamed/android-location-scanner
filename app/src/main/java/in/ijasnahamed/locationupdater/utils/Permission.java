package in.ijasnahamed.locationupdater.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

public class Permission {
    public static Boolean checkLocationPermission(Context context) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)
            return true;
        return (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }
}
