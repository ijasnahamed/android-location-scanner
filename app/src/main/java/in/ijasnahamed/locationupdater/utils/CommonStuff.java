package in.ijasnahamed.locationupdater.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CommonStuff {
    private static String TAG = "LocationUpdater";

    public static void Log(String message) {
        StackTraceElement stackTraceElement = new Exception().getStackTrace()[1];

        Log.i(TAG, GetDateTimeStamp()+"--"+stackTraceElement.getClassName()
                .replace(" com.nethram.smarthome.plugin.", "")
                +"("+stackTraceElement.getLineNumber()+")"
                +"."+stackTraceElement.getMethodName()
                +" => "+message);
    }

    public static void MakeToast(Context context, String message) {
        Log(message);
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    private static String GetDateTimeStamp()
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }
}
