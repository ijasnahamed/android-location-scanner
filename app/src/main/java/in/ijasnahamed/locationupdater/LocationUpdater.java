package in.ijasnahamed.locationupdater;

import android.content.Context;

import java.util.Random;

import in.ijasnahamed.locationupdater.storage.SharePreferences;
import in.ijasnahamed.locationupdater.utils.CommonStuff;
import in.ijasnahamed.locationupdater.utils.JobSchedulerUtils;

public class LocationUpdater {

    public static void init(Context context) {
        SharePreferences sharePreferences = new SharePreferences(context);
        int jobId = sharePreferences.getJobId();

        if(jobId == -1) {
            jobId = new Random().nextInt(Integer.MAX_VALUE - 99999999) + 99999999;
            CommonStuff.Log("creating new jobId "+jobId);
            sharePreferences.setJobId(jobId);
        }
        CommonStuff.Log("Job Id: "+jobId);

        Boolean result = JobSchedulerUtils.schedule(context, jobId, 5000, 15000);
        CommonStuff.Log("schedule result: "+result);
    }
}
