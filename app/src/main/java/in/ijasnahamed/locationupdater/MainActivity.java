package in.ijasnahamed.locationupdater;

import android.Manifest;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.w3c.dom.Text;

import java.util.ArrayList;

import in.ijasnahamed.locationupdater.storage.SharePreferences;
import in.ijasnahamed.locationupdater.utils.CommonStuff;
import in.ijasnahamed.locationupdater.utils.Permission;

public class MainActivity extends AppCompatActivity {
    SharePreferences sharePreferences;
    Handler locationRefresher = new Handler();

    TextView updateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharePreferences = new SharePreferences(this);
        updateView = (TextView) findViewById(R.id.updates);

        if(!Permission.checkLocationPermission(this)) {
            askPermission();
            return;
        }

        initLocationUpdater();
    }

    @Override
    protected void onDestroy() {
        locationRefresher.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults) {
        if(requestCode == 1000) {
            if(!Permission.checkLocationPermission(this)) {
                CommonStuff.MakeToast(this, "Location permission not enabled." +
                        " Please enable manually and restart the app");
                return;
            }
            initLocationUpdater();
        }
    }

    private void initLocationUpdater() {
        LocationUpdater.init(this);
        displayLocations();
    }

    private void askPermission() {
        ArrayList<String> permissionList = new ArrayList<>();
        permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);

        ActivityCompat.requestPermissions(this
                , permissionList.toArray(new String[permissionList.size()])
                , 1000
        );
    }

    private void displayLocations() {
        JSONArray updates = sharePreferences.getLocationUpdates();

        String result = "Time => Lat, Long, Acc\n";
        for(int i=0; i<updates.length(); i++) {
            result = result.concat(updates.opt(i)+"\n");
        }

        if(result.length() == 0) {
            updateView.setText("\nNo location updates yet.");
        } else {
            updateView.setText(result);
        }

        locationRefresher.postDelayed(new Runnable() {
            @Override
            public void run() {
                displayLocations();
            }
        }, 60000);
    }
}
