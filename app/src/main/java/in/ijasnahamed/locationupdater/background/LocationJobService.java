package in.ijasnahamed.locationupdater.background;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import in.ijasnahamed.locationupdater.storage.SharePreferences;
import in.ijasnahamed.locationupdater.utils.CommonStuff;
import in.ijasnahamed.locationupdater.utils.Permission;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class LocationJobService extends JobService {
    private static final int SCAN_DURATION = 1;

    private static JobParameters parameters = null;

    private SharePreferences sharePreferences = null;
    private FusedLocationProviderClient fusedLocationProviderClient = null;
    private Handler scanStopHandler = null;

    @Override
    public boolean onStartJob(JobParameters params) {
        if(parameters != null)
            return false;

        parameters = params;

        CommonStuff.Log("on Start job params: "+params.toString());
        CommonStuff.MakeToast(this, "on Start job");

        sharePreferences = new SharePreferences(this);
        int jobId = sharePreferences.getJobId();

        if(jobId != params.getJobId()) {
            parameters = null;
            return false;
        }

        JSONObject scanDetails = sharePreferences.getScanDetails();
        Boolean highAccuracy = scanDetails.optBoolean("accuracy", true);

        if(!initiateScan(highAccuracy)) {
            parameters = null;
            return false;
        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        CommonStuff.Log("on Stop job params: "+params.toString());
        CommonStuff.MakeToast(this, "on Start job");
        if(fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(callback);
        }

        if(scanStopHandler != null) {
            scanStopHandler.removeCallbacksAndMessages(null);
        }
        parameters = null;
        return false;
    }

    @NonNull
    private Boolean isLocationOn() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if(locationManager == null)
            return false;

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @NonNull
    @SuppressLint("MissingPermission")
    private Boolean initiateScan(Boolean highAccuracyScan) {
        if(!Permission.checkLocationPermission(this)) {
            CommonStuff.Log("No location permission. Stoping job service");
            return false;
        }

        if(!isLocationOn()) {
            CommonStuff.Log("Location turned off. Stoping job service");
            return false;
        }

        CommonStuff.Log("initiating scan");
        LocationRequest locationRequest = new LocationRequest();

        locationRequest.setInterval(SCAN_DURATION*50*1000);
        locationRequest.setFastestInterval((3*SCAN_DURATION*50*1000)/4);
        locationRequest.setMaxWaitTime((9*SCAN_DURATION*50*1000)/6);

        if(highAccuracyScan) {
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        } else {
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        if(fusedLocationProviderClient == null) {
            fusedLocationProviderClient
                    = LocationServices.getFusedLocationProviderClient(this);
        }

        fusedLocationProviderClient.removeLocationUpdates(callback);

        CommonStuff.Log("requesting scan");
        Task<Void> res = fusedLocationProviderClient
                .requestLocationUpdates(locationRequest, callback, null);

        CommonStuff.Log("scan result: "+res.isSuccessful());

        if(scanStopHandler == null) {
            scanStopHandler = new Handler(Looper.getMainLooper());
        }

        scanStopHandler.removeCallbacksAndMessages(null);
        scanStopHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, 570000);

        return true;
    }

    private void stopScan() {
        CommonStuff.Log("stopping scan");
        if(fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(callback);
        }

        if(scanStopHandler != null) {
            scanStopHandler.removeCallbacksAndMessages(null);
        }

        jobFinished(parameters, false);
        parameters = null;
    }

    private LocationCallback callback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult result) {
            if(result == null)
                return;

            List<Location> locations = result.getLocations();

            if(locations.size() == 0)
                return;

            JSONObject scanDetails = sharePreferences.getScanDetails();
            Boolean highAccuracyScan = scanDetails.optBoolean("accuracy", true);
            int count = scanDetails.optInt("count", 0);

            CommonStuff.Log("current scan Details: "+highAccuracyScan+", "+count);

            JSONArray previousUpdates = sharePreferences.getLocationUpdates();
            JSONArray newUpdates = new JSONArray();

            int start = 0;
            if(previousUpdates.length() + locations.size() > 10) {
                start = locations.size();
            }

            for (; start<previousUpdates.length() && start < 10; start++) {
                newUpdates.put(previousUpdates.optString(start));
            }

            Boolean accuracyChanged = false;
            for(int i=0; i<locations.size() && newUpdates.length() <= 10; i++) {
                Location location = locations.get(i);

                double latitude = location.getLatitude(), longitude = location.getLongitude()
                        , altitude = location.getAltitude();
                float locationAccuracy = location.getAccuracy(), speed = location.getSpeed()
                        , speedAccuracy = -1, verticalAccuracy = -1;
                String provider = location.getProvider();
                long time = location.getTime();

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    speedAccuracy = location.getSpeedAccuracyMetersPerSecond();
                    verticalAccuracy = location.getVerticalAccuracyMeters();
                }

                CommonStuff.Log("lat: "+latitude+", long: "+longitude+", accu: "+locationAccuracy
                        +", alt: "+altitude+", VAccu:"+verticalAccuracy
                        +", speed: "+speed+", SAccu: "+speedAccuracy
                        +", provider: "+provider+", time: "+time
                );

                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                String update = format.format(new Date(time)) +" => "+latitude+", "+longitude
                        +", "+locationAccuracy+", "+highAccuracyScan;
                CommonStuff.Log("New update: "+update);
                newUpdates.put(update);

                if(highAccuracyScan) {
                    if(locationAccuracy < 200)
                        count++;
                    else
                        count = 0;

                    if(count >= 5) {
                        highAccuracyScan = false;
                        count = 0;

                        accuracyChanged = true;
                    }
                } else {
                    if(locationAccuracy >= 200)
                        count++;
                    else
                        count = 0;

                    if(count >= 3) {
                        highAccuracyScan = true;
                        count = 0;

                        accuracyChanged = true;
                    }
                }
            }

            CommonStuff.Log("Resultant new update: "+newUpdates.toString());
            sharePreferences.setLocationUpdates(newUpdates);

            CommonStuff.Log("new scan Details: "+highAccuracyScan+", "+count);

            try {
                sharePreferences.setScanDetails(new JSONObject()
                        .put("accuracy", highAccuracyScan)
                        .put("count", count)
                );
            } catch (JSONException e) {
                CommonStuff.Log("exception: "+e.getMessage());
            }

            if(accuracyChanged) {
                if(!initiateScan(highAccuracyScan)) {
                    stopScan();
                    return;
                }
            }

            if(!Permission.checkLocationPermission(LocationJobService.this)) {
                CommonStuff.Log("No location permission. Stoping job service");
                stopScan();
                return;
            } else if(!isLocationOn()) {
                CommonStuff.Log("Location turned off. Stoping job service");
                stopScan();
                return;
            }
        }
    };
}
