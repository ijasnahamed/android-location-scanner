package in.ijasnahamed.locationupdater.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.location.LocationResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import in.ijasnahamed.locationupdater.storage.SharePreferences;
import in.ijasnahamed.locationupdater.utils.CommonStuff;

public class LocationUpdateReceiver extends BroadcastReceiver {
    public static final String ACTION = "locationUpdaterReceiverAction";
    public static final String ACCURACY_ACTION = "ServiceReceiverAction";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent == null)
            return;

        String action = intent.getAction();

        if(action == null)
            return;

        if(!action.equals(ACTION))
            return;

        LocationResult result = LocationResult.extractResult(intent);

        if(result == null)
            return;

        List<Location> locations = result.getLocations();

        if(locations.size() == 0)
            return;

        SharePreferences sharePreferences = new SharePreferences(context);

        JSONObject scanDetails = sharePreferences.getScanDetails();
        Boolean highAccuracyScan = scanDetails.optBoolean("accuracy", true);
        int count = scanDetails.optInt("count", 0);

        CommonStuff.Log("current scan Details: "+highAccuracyScan+", "+count);

        JSONArray previousUpdates = sharePreferences.getLocationUpdates();
        JSONArray newUpdates = new JSONArray();

        int start = 0;
        if(previousUpdates.length() + locations.size() > 10) {
            start = locations.size();
        }

        for (; start<previousUpdates.length() && start < 10; start++) {
            newUpdates.put(previousUpdates.optString(start));
        }

        for(int i=0; i<locations.size() && newUpdates.length() <= 10; i++) {
            Location location = locations.get(i);

            double latitude = location.getLatitude(), longitude = location.getLongitude()
                    , altitude = location.getAltitude();
            float locationAccuracy = location.getAccuracy(), speed = location.getSpeed()
                    , speedAccuracy = -1, verticalAccuracy = -1;
            String provider = location.getProvider();
            long time = location.getTime();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                speedAccuracy = location.getSpeedAccuracyMetersPerSecond();
                verticalAccuracy = location.getVerticalAccuracyMeters();
            }

            CommonStuff.Log("lat: "+latitude+", long: "+longitude+", accu: "+locationAccuracy
                    +", alt: "+altitude+", VAccu:"+verticalAccuracy
                    +", speed: "+speed+", SAccu: "+speedAccuracy
                    +", provider: "+provider+", time: "+time
            );

            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            String update = format.format(new Date(time)) +" => "+latitude+", "+longitude
                    +", "+locationAccuracy+", "+highAccuracyScan;
            CommonStuff.Log("New update: "+update);
            newUpdates.put(update);

//            Intent accuracyIntent = new Intent();
//            accuracyIntent.setAction(ACCURACY_ACTION);
//            accuracyIntent.putExtra("accuracy", locationAccuracy);
//            LocalBroadcastManager.getInstance(context).sendBroadcastSync(accuracyIntent);

            if(highAccuracyScan) {
                if(locationAccuracy < 200)
                    count++;
                else
                    count = 0;

                if(count >= 5) {
                    highAccuracyScan = false;
                    count = 0;
                }
            } else {
                if(locationAccuracy >= 200)
                    count++;
                else
                    count = 0;

                if(count >= 3) {
                    highAccuracyScan = true;
                    count = 0;
                }
            }
        }

        CommonStuff.Log("Resultant new update: "+newUpdates.toString());
        sharePreferences.setLocationUpdates(newUpdates);

        CommonStuff.Log("new scan Details: "+highAccuracyScan+", "+count);

        try {
            sharePreferences.setScanDetails(new JSONObject()
                    .put("accuracy", highAccuracyScan)
                    .put("count", count)
            );
        } catch (JSONException e) {
            CommonStuff.Log("exception: "+e.getMessage());
        }

//        LocationJobService.job.stop();
//        LocationJobService.job = null;
    }
}
