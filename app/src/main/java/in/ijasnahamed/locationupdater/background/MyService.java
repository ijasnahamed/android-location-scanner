package in.ijasnahamed.locationupdater.background;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import in.ijasnahamed.locationupdater.storage.SharePreferences;
import in.ijasnahamed.locationupdater.utils.CommonStuff;
import in.ijasnahamed.locationupdater.utils.Permission;

public class MyService extends Service {
    private int scanDuration = 1;
    private int count = 0;
    private Boolean highAccuracyScan = true;

    private FusedLocationProviderClient mFusedLocationClient;
    private PendingIntent locationUpdateIntent;
    private LocationManager locationManager;

    public MyService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        CommonStuff.MakeToast(this, "Service start");

        return START_NOT_STICKY;

//        SharePreferences sharePreferences = new SharePreferences(this);
//        Boolean libraryInitiated = sharePreferences.getLibraryInitiated();
//
//        if(!libraryInitiated) {
//            stopSelf();
//            return START_NOT_STICKY;
//        }
//
//        IntentFilter receiverFilter = new IntentFilter();
//        receiverFilter.addAction(LocationUpdateReceiver.ACCURACY_ACTION);
//        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, receiverFilter);
//
//        startLocationScan();
//
//        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        CommonStuff.MakeToast(this, "Service stop");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        if(mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(getLocationUpdateIntent());
        }
        super.onDestroy();
    }

    private Boolean isLocationOn() {
        if(locationManager == null) {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        }

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @NonNull
    private void startLocationScan() {
        if(!Permission.checkLocationPermission(this)) {
            CommonStuff.Log("No location permission. Stoping service");
            stopSelf();
            return;
        }

        if(!isLocationOn()) {
            CommonStuff.Log("Location turned off. Stoping service");
            stopSelf();
            return;
        }

        LocationRequest mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(scanDuration*60*1000);
        mLocationRequest.setFastestInterval((3*scanDuration*60*1000)/4);
        mLocationRequest.setMaxWaitTime(scanDuration*60*1000*2);

        if(highAccuracyScan) {
            CommonStuff.Log("starting high accuracy scan");
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        }
        else {
            CommonStuff.Log("starting balanced power accuracy scan");
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        if(mFusedLocationClient == null)
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.removeLocationUpdates(getLocationUpdateIntent());
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, getLocationUpdateIntent());

        count = 0;
    }

    private PendingIntent getLocationUpdateIntent() {
        if(locationUpdateIntent != null)
            return locationUpdateIntent;

        Intent intent = new Intent(this, LocationUpdateReceiver.class);
        intent.setAction(LocationUpdateReceiver.ACTION);

        locationUpdateIntent = PendingIntent.getBroadcast(this, 0
                , intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return locationUpdateIntent;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!isLocationOn()) {
                CommonStuff.Log("location not on. Stoping service");
                stopSelf();
                return;
            }

            if(intent == null)
                return;

            String action = intent.getAction();

            if(action == null)
                return;

            if(!action.equals(LocationUpdateReceiver.ACCURACY_ACTION))
                return;

            float accuracy = intent.getFloatExtra("accuracy", -1);

            if(accuracy < 0)
                return;

            if(highAccuracyScan) {
                if(accuracy < 200)
                    count++;
                else
                    count = 0;

                if(count >= 5) {
                    highAccuracyScan = false;
                    startLocationScan();
                }
            } else {
                if(accuracy >= 200)
                    count++;
                else
                    count = 0;

                if(count >= 3) {
                    highAccuracyScan = true;
                    startLocationScan();
                }
            }

            CommonStuff.Log("accuracy receiver -> highAccuracy: "+highAccuracyScan
                    +", count: "+count+", accuracy: "+accuracy);
        }
    };
}
