package in.ijasnahamed.locationupdater.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import in.ijasnahamed.locationupdater.storage.SharePreferences;
import in.ijasnahamed.locationupdater.utils.CommonStuff;
import in.ijasnahamed.locationupdater.utils.ServiceUtils;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent == null)
            return;

        String action = intent.getAction();

        if(action == null)
            return;

        CommonStuff.Log("Inside Boot receiver with action "+action);

        if(!action.equals(Intent.ACTION_BOOT_COMPLETED)
                && !action.equals("android.location.PROVIDERS_CHANGED"))
            return;

        CommonStuff.Log("Boot or provider change receiver");

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if(locationManager == null)
            return;

        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return;

//        SharePreferences sharePreferences = new SharePreferences(context);
//        if(sharePreferences.getLibraryInitiated()
//                && !ServiceUtils.isServiceRunning(context, MyService.class)) {
//            ServiceUtils.startService(context, MyService.class);
//        }
    }
}
